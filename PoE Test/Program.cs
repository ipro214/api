﻿using System;
using System.Threading;
using System.Collections.Generic;
using Igor.Gateway.Api.Sdk.Apis.Lights;
using Igor.Gateway.Api.Sdk.Apis.Actions;

namespace PoE_Test {
    class Program {
        static LightService lightService;
        static ActionService actionService;
        static int strobeDelay = 1000;

        /// <summary>
        ///  This program splits the TS 2030 PoE lights into two categories: "hallway" and "classroom" lights
        ///  and turns off classroom lights and uses "hallway" lights to guide people out of the room 
        ///  runway-lights-style.
        /// </summary>
        /// <param name="args">This program doesn't do anything with its arguments</param>
        static void Main(string[] args) {
            ////Small system API key and IP address
            //var service = new LightService("0720961740304db48637460b97d45680", "http://192.168.2.100");
            
            //Classroom (TS 2030) system API key and IP address
            lightService = new LightService("a0af07fb613e42558918ebcc76480828", "http://192.168.10.2");
            actionService = new ActionService("a0af07fb613e42558918ebcc76480828", "http://192.168.10.2");

            //Light Ids of the "hallway" lights in first-to-turn-on to last-to-turn-on strobe order
            int[] hallIds = { 3, 10, 12, 11, 6, 9, 1 };

            /* Used by the fastBlinkLight function.
             * Array of Array of Action Ids of the "hallway" lights, 
             * first index corresponds to indices in hallIds 
             * second index is whether you want the offID=0 or onId=1 */
            int[][] hallActionIds = new int[7][]; // array of lights' [offID, onID]
            hallActionIds[0] = new int[] { 67, 68 };
            hallActionIds[1] = new int[] { 69, 70 };
            hallActionIds[2] = new int[] { 65, 66 };
            hallActionIds[3] = new int[] { 71, 72 };
            hallActionIds[4] = new int[] { 73, 74 };
            hallActionIds[5] = new int[] { 75, 76 };
            hallActionIds[6] = new int[] { 77, 78 };

            //Light Ids of the "classroom" lights
            int[] classroomIds = { 8, 7, 5, 4, 2 };
            
            //Turn "classroom" lights off upon program initiation
            foreach (int roomId in classroomIds)
            {
                lightService.TurnOff(roomId);
            }

            List<Thread> threads = new List<Thread>();
            int idIndex = 0;
            int betweenDelay = 100 + strobeDelay / hallIds.Length;

            Console.WriteLine("Press Enter to stop lights");
            //Loop through all the "hallway" lights to start threads for them to cause the runway-lights effect
            do
            {
                while (!Console.KeyAvailable) {
                    ////Light turn on and off at the default speed
                    //Thread blinkThread = new Thread(new ParameterizedThreadStart(blinkLight));
                    //blinkThread.Start(hallIds[idIndex]);
                    //threads.Add(blinkThread);

                    // Uses pre-made turn off and turn on actions that are grouped in 
                    // the "Fast Light Control Fall 2018 code" Action Set
                    Thread fastBlinkThread = new Thread(new ParameterizedThreadStart(fastBlinkLight));
                    fastBlinkThread.Start(hallActionIds[idIndex]);
                    Thread.Sleep(betweenDelay);
                    if (++idIndex >= hallIds.Length) {
                        idIndex = 0;
                    }
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Enter);

            //Wait for light threads to finish (all "hallway" lights will be on when they complete)
            Console.WriteLine("Waiting for threads to complete.");
            foreach (Thread thread in threads) {
                thread.Join();
            }

            //Turn "classroom" lights on upon program completion
            foreach (int roomId in classroomIds)
            {
                lightService.TurnOn(roomId);
            }
            Console.WriteLine("Program complete. Press any key to close.");
            Console.ReadKey();
        }

        /// <summary>
        /// Blinks lights using the default TurnOff and TurnOn actions for lights,
        /// which doesn't happen quickly (fades off, turns off fast but not instantaneously).
        /// </summary>
        /// <param name="arg">An object type that is actually an int lightId</param>
        static void blinkLight(object arg) {
            int id = (int)arg;
            //Console.WriteLine(arg + " Off.");
            lightService.TurnOff(id);
            Thread.Sleep(strobeDelay);
            //Console.WriteLine(arg + " On.");
            lightService.TurnOn(id);
        }

        /// <summary>
        /// The light whose actions are passed turn on and off instantaneously. 
        /// Uses pre-made turn off and turn on actions individually that are grouped in 
        /// the "Fast Light Control Fall 2018 code" Action Set, which has an Id of 43.
        /// </summary>
        /// <param name="arg">An object that is actually a int[] {turnOffActionId, turnOnActionId}</param>
        static void fastBlinkLight(object arg) {
            int[] args = (int[])arg;
            //turn light off
            actionService.Execute(43, args[0]);
            Thread.Sleep(strobeDelay);
            //turn light on
            actionService.Execute(43, args[1]);
            //Console.WriteLine("On.");
        }
    }
}
